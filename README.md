# README #

* This UI Library is for whoever work in a TrueYou EDC Project.
* This UI Library is working with Sketch.app
* How to add this UI Library in Sketch.app
	1. Go to Sketch Menu on top left
	2. Select Menu "Perferences"
	3. Select Tab "Libraries"
	4. Press Button "Add Library..."
	5. Done!!! Now you can use this UI Library from Symbol Tool.

### What is this repository for? ###

- Get an UI Library Update
- Feel free to request (Create Issues) for request NEW, UPDATE or FIX this UI Library.

### Who do I talk to? ###

* Worapol Thipmaneemongkol (UI Designer @ EGG Digital)
* Worapol.thi@ascendcorp.com

![alt text](http://kirakirakyoto.com/files/ui-library.png) 
